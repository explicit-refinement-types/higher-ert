import Mathlib.Order.Monotone.Basic
import Mathlib.Init.Function
import Mathlib.Logic.Function.Basic
import Aesop

/--A weakening, corresponding to a monotonic map from `1..n` to `1..m` where `m >= n`-/

inductive Wk: Type where
  | /-- The identity weakening, which maps all integers to themselves -/
  id: Wk
  | /-- `ρ.step` maps `n` to `(ρ n) + 1` -/
  step: Wk -> Wk
  | /-- `ρ.lift` leaves `0` unchanged and maps `n + 1` to `(ρ n) + 1` -/
  lift: Wk -> Wk
deriving Inhabited, DecidableEq, Repr

/--A compressed weakening, corresponding to a monotonic map from `1..n` to `1..m` where `m >= m`
This type is more efficient than `Wk`, but is more complex-/
inductive CWk: Type where
  | /-- The identity weakening, which maps all integers to themselves -/
  id
  | /-- `ρ.shift n m` leaves `0..m` unchanged and maps `m + k` to `(ρ k) + n`  -/
  shift (n: Nat) (m: Nat) (ρ: CWk)
deriving Inhabited, DecidableEq, Repr

/-- The weakening mapping `n` to `n + 1` -/
@[aesop unsafe 30%]
abbrev Wk.wk1: Wk := step id
/-- The weakening mapping `n` to `n + 1` -/
abbrev CWk.wk1: CWk := shift 1 0 id

/-- `ρ.stepn n` maps numbers `k` to `(ρ k) + n`` -/
def Wk.stepn: Wk -> Nat -> Wk
  | ρ, 0 => ρ
  | ρ, Nat.succ n => Wk.step (ρ.stepn n)

@[aesop unsafe 60%]
theorem Wk.stepn_0 (ρ: Wk): ρ.stepn 0 = ρ := rfl
@[aesop unsafe 60%]
theorem Wk.stepn_1 (ρ: Wk): ρ.stepn 1 = ρ.step := rfl
@[aesop unsafe 60%]
theorem Wk.stepn_succ (ρ: Wk) (n: Nat): ρ.stepn (n + 1) = (ρ.stepn n).step := rfl
@[aesop unsafe 30%]
theorem Wk.stepn_step (ρ: Wk) (n: Nat): (ρ.stepn n).step = ρ.stepn (n + 1) := rfl
@[aesop unsafe 60%]
theorem Wk.step_stepn (ρ: Wk) (n: Nat): (ρ.step).stepn n = ρ.stepn (n + 1)
  := by induction n <;> simp [stepn, *]
@[aesop unsafe 30%]
theorem Wk.step_stepn_comm (ρ: Wk) (n: Nat): (ρ.step).stepn n = (ρ.stepn n).step
  := by rw [step_stepn, stepn_step]
@[aesop unsafe 50%]
theorem Wk.stepn_add (ρ: Wk) (n m: Nat): (ρ.stepn n).stepn m = ρ.stepn (n + m)
  := by induction m <;> simp [stepn, *]
@[aesop unsafe 30%]
theorem Wk.stepn_comm (ρ: Wk) (n m: Nat): (ρ.stepn n).stepn m = (ρ.stepn m).stepn n
  := by simp only [stepn_add, Nat.add_comm]

/-- `ρ.liftn n` is the identity on `0..n` and maps numbers `n + k` to `(ρ k) + n` -/
def Wk.liftn: Wk -> Nat -> Wk
  | ρ, 0 => ρ
  | ρ, Nat.succ n => Wk.lift (ρ.liftn n)

@[aesop unsafe 60%]
theorem Wk.liftn_0 (ρ: Wk): ρ.liftn 0 = ρ := rfl
@[aesop unsafe 60%]
theorem Wk.liftn_1 (ρ: Wk): ρ.liftn 1 = ρ.lift := rfl
@[aesop unsafe 60%]
theorem Wk.liftn_succ (ρ: Wk) (n: Nat): ρ.liftn (n + 1) = (ρ.liftn n).lift := rfl
@[aesop unsafe 30%]
theorem Wk.liftn_lift (ρ: Wk) (n: Nat): (ρ.liftn n).lift = ρ.liftn (n + 1) := rfl
@[aesop unsafe 60%]
theorem Wk.lift_liftn (ρ: Wk) (n: Nat): (ρ.lift).liftn n = ρ.liftn (n + 1)
  := by induction n <;> simp [liftn, *]
@[aesop unsafe 30%]
theorem Wk.lift_liftn_comm (ρ: Wk) (n: Nat): (ρ.lift).liftn n = (ρ.liftn n).lift
  := by rw [lift_liftn, liftn_lift]
@[aesop unsafe 50%]
theorem Wk.liftn_add (ρ: Wk) (n m: Nat): (ρ.liftn n).liftn m = ρ.liftn (n + m)
  := by induction m <;> simp [liftn, *]
@[aesop unsafe 30%]
theorem Wk.liftn_comm (ρ: Wk) (n m: Nat): (ρ.liftn n).liftn m = (ρ.liftn m).liftn n
  := by simp only [liftn_add, Nat.add_comm]

/-- `wkn m` maps `n` to `n + m`; equivalent to applying `wk1` `m` times -/
abbrev Wk.wkn (n: Nat): Wk := id.stepn n

@[aesop safe]
theorem Wk.wkn_0: wkn 0 = id := rfl
@[aesop safe]
theorem Wk.wkn_1: wkn 1 = wk1 := rfl
@[aesop safe]
theorem Wk.wk1_wkn1: wk1 = wkn 1 := rfl
@[aesop safe]
theorem Wk.wkn_succ (n: Nat): wkn (n + 1) = (wkn n).step := rfl
@[aesop safe]
theorem Wk.wkn_step (n: Nat): (wkn n).step = wkn (n + 1) := rfl
@[aesop safe]
theorem Wk.wkn_stepn (n m: Nat): (wkn n).stepn m = wkn (n + m)
  := by rw [stepn_add]

/-- `ρ.wknth` is the identity on `0..n` and maps numbers `n + k` to `n + k + 1`  -/
abbrev Wk.wknth (n: Nat): Wk := wk1.liftn n

theorem Wk.wknth_0: wknth 0 = wk1 := rfl
theorem Wk.wknth_succ (n: Nat): wknth (n + 1) = (wknth n).lift := rfl
theorem Wk.wknth_lift (n: Nat): (wknth n).lift = wknth (n + 1) := rfl
theorem Wk.wknth_liftn (n m: Nat): (wknth n).liftn m = wknth (n + m)
  := by rw [liftn_add]

/-- Apply a weakening to a natural number -/
def Wk.app: Wk -> Nat -> Nat
  | Wk.id, n => n
  | Wk.step ρ, n => (app ρ n) + 1
  | Wk.lift _, 0 => 0
  | Wk.lift ρ, (n + 1) => (app ρ n) + 1

@[aesop safe]
theorem Wk.app_id {n}: id.app n = n := rfl
@[aesop safe]
theorem Wk.app_step {n} (ρ: Wk): ρ.step.app n = (ρ.app n).succ := rfl
@[aesop safe]
theorem Wk.app_wk1 {n}: wk1.app n = n.succ := rfl
@[aesop safe]
theorem Wk.app_wkn {n m}: (wkn n).app m = m + n := by induction n with
  | zero => rfl
  | succ _ I => simp only [wkn, stepn, app_step, Nat.add_succ, I]
theorem Wk.app_step_fn (ρ: Wk): ρ.step.app = λx => (ρ.app x).succ := rfl
@[aesop safe]
theorem Wk.app_lift_0 (ρ: Wk): ρ.lift.app 0 = 0 := rfl
@[aesop safe]
theorem Wk.app_lift_1 (ρ: Wk): ρ.lift.app 1 = (ρ.app 0).succ := rfl
@[aesop safe]
theorem Wk.app_lift_succ {n} (ρ: Wk): ρ.lift.app (n + 1) = (ρ.app n).succ := rfl
@[aesop safe]
theorem Wk.app_lift {n} (ρ: Wk): ρ.lift.app n = match n with | 0 => 0 | n + 1 => (app ρ n) + 1
  := by cases n <;> rfl
@[aesop safe]
theorem Wk.app_lift_fn (ρ: Wk)
  : ρ.lift.app = λn => match n with | 0 => 0 | n + 1 => (app ρ n) + 1
  := by funext n; rw [app_lift]
@[aesop safe]
theorem Wk.app_liftn {n v} (ρ: Wk): (ρ.liftn n).app v = if v < n then v else (app ρ (v - n)) + n
  := by induction n generalizing v ρ with
     | zero => simp_arith [liftn]
     | succ n I =>
      rw [liftn, app_lift]
      cases v with
      | zero => simp_arith
      | succ v =>
        simp only [I]
        split
        simp_arith only []
        split
        rfl
        contradiction
        simp_arith only []
        split
        contradiction
        simp_arith

theorem Wk.strictMono (ρ: Wk): StrictMono ρ.app
  := λn m H => match ρ with
  | id => H
  | step ρ => Nat.succ_lt_succ (strictMono ρ H)
  | lift ρ => by
    cases n <;> cases m <;> try contradiction
    exact Nat.zero_lt_succ _
    exact Nat.succ_lt_succ (strictMono ρ (Nat.lt_of_succ_lt_succ H))

theorem Wk.monotone (ρ: Wk): Monotone ρ.app
  := λn m H => by rw [ρ.strictMono.le_iff_le]; exact H

theorem Wk.injective (ρ: Wk): Function.Injective ρ.app
  := by rw [<-ρ.monotone.strictMono_iff_injective]; exact ρ.strictMono

@[aesop safe]
theorem Wk.larger (ρ: Wk) (n: Nat): n ≤ ρ.app n
  := match ρ with
  | id => n.le_refl
  | step ρ => Nat.le_trans (larger ρ n) (Nat.le_succ _)
  | lift ρ => match n with
              | 0 => Nat.le_refl 0
              | n + 1 => Nat.succ_le_succ (larger ρ n)

/-- Compose two weakenings -/
def Wk.comp: Wk -> Wk -> Wk
    | Wk.id, σ => σ
    | Wk.step ρ, σ => Wk.step (comp ρ σ)
    | Wk.lift ρ, Wk.id => Wk.lift ρ
    | Wk.lift ρ, Wk.step σ => Wk.step (comp ρ σ)
    | Wk.lift ρ, Wk.lift σ => Wk.lift (comp ρ σ)

@[aesop safe] theorem Wk.id_comp (ρ: Wk): id.comp ρ = ρ := rfl
@[aesop safe] theorem Wk.comp_id (ρ: Wk): ρ.comp id = ρ
  := by induction ρ <;> simp [comp, *]
@[aesop safe] theorem Wk.wk1_comp (ρ: Wk): wk1.comp ρ = ρ.step := rfl
@[aesop safe] theorem Wk.comp_assoc (ρ σ τ: Wk): (ρ.comp σ).comp τ = ρ.comp (σ.comp τ)
  := by {
    induction ρ generalizing σ τ <;>
    simp only [comp] <;>
    cases σ <;> cases τ <;>
    simp only [comp, *]
  }
@[aesop safe] theorem Wk.lift_comp_lift {ρ σ: Wk}: (ρ.lift).comp (σ.lift) = (ρ.comp σ).lift
  := rfl
@[aesop safe] theorem Wk.liftn_comp_liftn {ρ σ: Wk} {n}: (ρ.liftn n).comp (σ.liftn n) = (ρ.comp σ).liftn n
  := by induction n generalizing ρ σ with
    | zero => rfl
    | succ => simp only [liftn_succ, lift_comp_lift, *]
@[aesop safe]
theorem Wk.lift_dist_comp {ρ σ: Wk}: (ρ.comp σ).lift = (ρ.lift).comp (σ.lift)
  := lift_comp_lift.symm
@[aesop safe]
theorem Wk.liftn_dist_comp {ρ σ: Wk} {n}: (ρ.comp σ).liftn n = (ρ.liftn n).comp (σ.liftn n)
  := liftn_comp_liftn.symm


@[aesop safe]
theorem Wk.wkn_comp_wk1 (n: Nat): (wkn n).comp wk1 = wkn (n + 1)
  := by induction n <;> simp only [wkn_succ, comp, *]
@[aesop safe]
theorem Wk.app_comp (ρ σ: Wk) (n: Nat): Wk.app (Wk.comp ρ σ) n = Wk.app ρ (Wk.app σ n)
  := by {
    induction ρ generalizing σ n <;> cases σ <;> cases n <;> simp [app, *]
  }

def Wk.equiv (ρ σ: Wk) := ρ.app = σ.app

theorem Wk.equiv_refl (ρ: Wk): ρ.equiv ρ := rfl
theorem Wk.equiv.symm {ρ σ: Wk}: ρ.equiv σ -> σ.equiv ρ := Eq.symm
theorem Wk.equiv.trans {ρ σ τ: Wk}: ρ.equiv σ -> σ.equiv τ -> ρ.equiv τ := Eq.trans
theorem Wk.equiv.app {ρ σ: Wk}: ρ.equiv σ -> ∀n, ρ.app n = σ.app n := λH n => by rw [H]
theorem Wk.equiv.ext {ρ σ: Wk} (H: ∀n, ρ.app n = σ.app n): ρ.equiv σ := by funext n; apply H

def Wk.coherent_inner (f: Wk -> Wk) := ∀{ρ σ: Wk}, ρ.equiv σ -> (f ρ).equiv (f σ)
def Wk.coherent {A} (f: Wk -> A) := ∀{ρ σ: Wk}, ρ.equiv σ -> f ρ = f σ

@[aesop safe]
theorem Wk.equiv.step: coherent_inner step
  := λH => by funext n; simp only [Wk.app]; rw [H]
@[aesop unsafe 50%]
theorem Wk.equiv.step_aesop_helper: ∀{ρ σ: Wk}, ρ.equiv σ -> (Wk.step ρ).equiv (Wk.step σ)
  := Wk.equiv.step
@[aesop safe]
theorem Wk.equiv.lift: coherent_inner lift
  := λH => by funext n; cases n <;> simp only [Wk.app]; rw [H]
@[aesop unsafe 50%]
theorem Wk.equiv.lift_aesop_helper: ∀{ρ σ: Wk}, ρ.equiv σ -> (Wk.lift ρ).equiv (Wk.lift σ)
  := Wk.equiv.lift
@[aesop unsafe 95%]
theorem Wk.equiv.stepn {ρ σ: Wk} {n} (H: ρ.equiv σ): (ρ.stepn n).equiv (σ.stepn n)
  := by induction n with | zero => exact H | succ _ I => exact I.step
@[aesop unsafe 95%]
theorem Wk.equiv.liftn {ρ σ: Wk} {n} (H: ρ.equiv σ): (ρ.liftn n).equiv (σ.liftn n)
  := by induction n with | zero => exact H | succ _ I => exact I.lift
@[aesop unsafe 50%]
theorem Wk.equiv.unstep {ρ σ: Wk} (H: ρ.step.equiv σ.step): ρ.equiv σ
  := by funext n; apply Nat.succ.inj; simp only [<-Wk.app_step]; rw [H]
@[aesop unsafe 50%]
theorem Wk.equiv.unlift {ρ σ: Wk} (H: ρ.lift.equiv σ.lift): ρ.equiv σ
  := by funext n; have H' := congr H (@rfl _ (n + 1)); apply Nat.succ.inj; exact H'
@[aesop unsafe 60%]
theorem Wk.equiv.unstepn {ρ σ: Wk} {n} (H: (ρ.stepn n).equiv (σ.stepn n)): ρ.equiv σ
  := by induction n with | zero => exact H | succ _ I => exact I (H.unstep)
@[aesop unsafe 60%]
theorem Wk.equiv.unliftn {ρ σ: Wk} {n} (H: (ρ.liftn n).equiv (σ.liftn n)): ρ.equiv σ
  := by induction n with | zero => exact H | succ _ I => exact I (H.unlift)
@[aesop safe]
theorem Wk.lift_id_equiv_id: id.lift.equiv id := by funext n; cases n <;> rfl
theorem Wk.equiv.not_step {ρ: Wk}: ¬(ρ.step.equiv id)
  := Function.ne_iff.mpr ⟨0, by simp [Wk.app]⟩
@[aesop unsafe 95%]
theorem Wk.equiv.lift_equiv_id {ρ: Wk} (H: ρ.equiv id): ρ.lift.equiv id
  := H.lift.trans lift_id_equiv_id
@[aesop unsafe 95%]
theorem Wk.equiv.liftn_equiv_id {ρ: Wk} (H: ρ.equiv id) (n: Nat): (ρ.liftn n).equiv id
  := by induction n with | zero => exact H | succ n I => rw [liftn_succ]; exact I.lift_equiv_id
@[aesop unsafe 10%]
theorem Wk.equiv.unlift_equiv_id {ρ: Wk} (H: ρ.lift.equiv id): ρ.equiv id
  := (H.trans lift_id_equiv_id.symm).unlift
@[aesop unsafe 30%]
theorem Wk.equiv.equiv_id_decomp {ρ: Wk} (H: ρ.equiv id): ∃n, ρ = id.liftn n
  := match ρ with
    | Wk.id => ⟨0, rfl⟩
    | Wk.step _ => H.not_step.elim
    | Wk.lift ρ =>
      let ⟨n, p⟩ := H.unlift_equiv_id.equiv_id_decomp; ⟨n.succ, by rw [liftn_succ, p]⟩
theorem Wk.equiv_id_char (ρ: Wk): ρ.equiv id ↔ ∃n, ρ = id.liftn n :=
  ⟨Wk.equiv.equiv_id_decomp, λ⟨n, E⟩ => E ▸ id.equiv_refl.liftn_equiv_id n⟩

/-- The source range `0..n` of a weakening -/
def Wk.src: Wk -> Nat
  | id => 0
  | step ρ => ρ.src
  | lift ρ => ρ.src + 1

/-- The target range `0..m` of a weakening -/
def Wk.trg: Wk -> Nat
  | id => 0
  | step ρ => ρ.trg + 1
  | lift ρ => ρ.trg + 1

/-- The minimal source range `0..n` of a weakening -/
def Wk.min_src: Wk -> Nat
  | id => 0
  | step ρ => ρ.min_src
  | lift ρ => match ρ.min_src with | 0 => 0 | n => n + 1

/-- The minimal target range `0..m` of a weakening -/
def Wk.min_trg: Wk -> Nat
  | id => 0
  | step ρ => ρ.min_trg
  | lift ρ => match ρ.min_trg with | 0 => 0 | n => n + 1

theorem Wk.src_le_trg (ρ: Wk): ρ.src ≤ ρ.trg
  := by induction ρ with
  | id => simp
  | step => simp only [Wk.src, Wk.trg]; apply Nat.le_of_succ_le; apply Nat.succ_le_succ; assumption
  | lift => simp only [Wk.src, Wk.trg]; apply Nat.succ_le_succ; assumption

-- theorem Wk.min_src_le_src (ρ: Wk): ρ.min_src ≤ ρ.src := sorry

-- theorem Wk.min_trg_le_src (ρ: Wk): ρ.min_trg ≤ ρ.trg := sorry

-- theorem Wk.min_src_le_min_trg (ρ: Wk): ρ.min_src ≤ ρ.min_trg
--   := by induction ρ with
--   | id => simp
--   | step ρ => assumption
--   | lift ρ =>
--     simp only [Wk.min_src, Wk.min_trg]
--     sorry

-- theorem Wk.min_src_coherent: Wk.coherent min_src := sorry

-- theorem Wk.min_trg_coherent: Wk.coherent min_trg := sorry

def Wk.app_inv: Wk -> Nat -> Nat
  | Wk.id, n => n
  | Wk.step _, 0 => 0
  | Wk.step ρ, n + 1 => (app_inv ρ n)
  | Wk.lift _, 0 => 0
  | Wk.lift ρ, (n + 1) => (app_inv ρ n) + 1

@[aesop safe]
theorem Wk.app_inv_inv (ρ: Wk) (n: Nat): ρ.app_inv (ρ.app n) = n
  := by induction ρ generalizing n with
  | id => rfl
  | step _ I => exact I _
  | lift ρ I => cases n
    <;> simp only [app_inv, Nat.add_zero, Nat.add, I _]

@[aesop safe]
theorem Wk.wk1_app_inv (n: Nat): wk1.app_inv n = n - 1 := by cases n <;> aesop
@[aesop safe]
theorem Wk.wkn_app_inv (n m: Nat): (wkn n).app_inv m = m - n :=
  by induction n generalizing m <;> cases m <;> aesop
