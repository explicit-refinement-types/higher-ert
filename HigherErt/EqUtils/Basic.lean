--TODO: go search for something like this in mathlib...
theorem rec_to_cast:
  @Eq.rec A x F D y p =
  cast (by cases p; rfl) D
  := by {
    cases p;
    rfl
  }

theorem heq_rec_left (a: α) (p: α = β): HEq (p ▸ a) a
  := by cases p; simp
theorem heq_rec_right (a: α) (p: α = β): HEq a (p ▸ a)
  := by cases p; simp
theorem heq_rec_both (a: α) (p: α = β) (q: α = γ): HEq (p ▸ a) (q ▸ a)
  := by cases p; cases q; simp
theorem heq_rec_left' (a: α) (b: β) (p: α = β) (H: b = p ▸ a): HEq b a
  := by cases p; cases H; simp
theorem heq_rec_right' (a: α) (b: β) (p: α = β) (H: b = p ▸ a): HEq a b
  := by cases p; cases H; simp
