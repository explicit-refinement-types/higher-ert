import HigherErt.VarUtils.Wk
import HigherErt.Syntax.Term
import HigherErt.Syntax.Wk

def Term.Subst: Type := Nat -> Term

def Term.Subst.lift (σ: Subst): Subst
  := λv => match v with | 0 => var 0 | v + 1 => (σ v).wk1
def Term.Subst.liftn (σ: Subst) (n: Nat): Subst
  := λv => if v < n then var v else (σ (v - n)).wkn n
def Term.Subst.liftn' (σ: Subst): Nat -> Subst
  | 0 => σ
  | n + 1 => (σ.liftn' n).lift

theorem Term.Subst.liftn0 (σ: Subst): σ.liftn 0 = σ
  := by funext v; simp_arith [liftn, Term.wkn0]
--TODO: clean this proof up...
theorem Term.Subst.liftn_succ (σ: Subst) (n: Nat): σ.liftn n.succ = (σ.liftn n).lift
  := by induction n with
    | zero => funext v; cases v <;> simp_arith [lift, liftn]
    | succ n I =>
      rw [I]; funext v; cases v <;> simp_arith [lift, liftn]
      split
      {
        split
        rfl
        simp_arith [*]
        rfl
      }
      {
        split
        have H' := n.zero_le; contradiction
        split
        contradiction
        simp_arith
      }
theorem Term.Subst.liftn1 (σ: Subst): σ.liftn 1 = σ.lift
  := by rw [liftn_succ, liftn0]
theorem Term.Subst.liftn_eq_liftn' (σ: Subst): σ.liftn = σ.liftn'
  := by funext n; induction n with
    | zero => rw [liftn0]; rfl
    | succ _ I => rw [liftn_succ, I]; rfl
theorem Term.Subst.liftn'_liftn' (σ: Subst) (n m: Nat): (σ.liftn' n).liftn' m = (σ.liftn' (n + m))
  := by induction m <;> simp only [liftn', *] <;> rfl
theorem Term.Subst.liftn_liftn (σ: Subst) (n m: Nat): (σ.liftn n).liftn m = (σ.liftn (n + m))
  := by simp only [liftn'_liftn', liftn_eq_liftn']
theorem Term.Subst.liftn_liftn_comm (σ: Subst) (n m: Nat): (σ.liftn n).liftn m = (σ.liftn m).liftn n
  := by simp only [liftn_liftn, Nat.add_comm]
theorem Term.Subst.lift_liftn_comm (σ: Subst) (n: Nat): σ.lift.liftn n = (σ.liftn n).lift
  := by simp only [<-liftn1, liftn_liftn_comm]

def Term.Subst.to_term_fold (σ: Subst) (b: Nat): Term.Shape Term -> Term
  | Term.Shape.var n => (σ.liftn b) n
  | t => t.to_term

abbrev Term.subst_base (t: Term) (n: Nat) (σ: Subst): Term
  := t.fold_base n σ.to_term_fold
theorem Term.subst_base_var (v n: Nat) (σ: Subst)
  : (var v).subst_base n σ = (σ.liftn n) v
  := rfl

abbrev Term.subst (t: Term) (σ: Subst): Term
  := t.subst_base 0 σ
theorem Term.subst_var (v: Nat) (σ: Subst)
  : (var v).subst σ = σ v
  := by rw [subst, subst_base_var, Subst.liftn0]
theorem Term.subst_base_succ (t: Term) (n: Nat) (σ: Subst)
  : t.subst_base (n + 1) σ = t.subst_base n σ.lift
  := by induction t generalizing n σ with
     | var =>
      simp only [subst_base_var, subst_var, Term.Subst.liftn_succ, Term.Subst.lift_liftn_comm]
     | _ => simp only [fold_base, *]; rfl
theorem Term.subst_base_char (t: Term) (n: Nat) (σ: Subst):
  t.subst_base n σ = t.subst (σ.liftn n)
  := by induction n generalizing t σ with
     | zero => rw [Subst.liftn0]
     | succ n => simp only [subst_base_succ, Term.Subst.liftn_succ, Term.Subst.lift_liftn_comm, *]

def Term.Subst.comp (σ τ: Subst): Subst := λv => (τ v).subst σ

theorem Term.subst_base_wk1 (t: Term) (b w: Nat) (σ: Subst)
  : (t.subst_base (b + w) σ).wk1_base w = (t.wk1_base w).subst_base (b + w + 1) σ
  := by induction t generalizing b w σ with
     | var n =>
      simp only [subst_base_var, wk_base_var, Subst.liftn, Wk.app_liftn, Wk.app_wk1]
      split
      split
      simp_arith [wk_base_var, Wk.app_liftn, Wk.app_wk1, Wk.to_term_fold, *]
      split
      rfl
      have _: n ≤ b + w := Nat.le_of_lt (by assumption);
      contradiction
      have H: n ≥ w := Nat.le_of_not_lt (by assumption);
      simp_arith only [Nat.succ_add, Nat.sub_add_cancel H]
      split
      simp only [
        fold_base, Wk.to_term_fold, Wk.app_liftn, Wk.app_wk1,
        Nat.succ_add, Nat.sub_add_cancel H
      ]
      split
      contradiction
      rfl
      contradiction
      have Hnbw: n ≥ b + w := Nat.le_of_not_lt (by assumption);
      have Hnw: n ≥ w := Nat.le_trans (Nat.le_add_left w b) Hnbw;
      have Hnw': ¬n < w := Nat.not_lt_of_le Hnw;
      split
      contradiction
      simp_arith only [Nat.sub_add_cancel Hnw, Nat.succ_add]
      split
      contradiction
      simp only [wkn_wk1, Nat.add_succ, Nat.add_zero, Nat.succ_sub_succ, wk1, wkn, wk, wk1_base]
      rw [wk_base_char, wk_base_char, wk_base_char, <-wk_comp, <-wk_comp, wk_coherent]
      funext v;
      simp_arith only [Wk.app_comp, Wk.liftn_0, Wk.app_wk1, Wk.app_wkn, Wk.app_liftn]
      split
      contradiction
      rw [<-Nat.add_assoc, Nat.add_sub_cancel]
      simp_arith
     | _ =>
        have H: ∀n, b + w + n = b + (w + n) := by simp_arith;
        simp only [subst_base, wk_base, wk1_base, fold_base, H, *]
        rfl
theorem Term.subst_base_wk1' (t: Term) (b w: Nat) (σ: Subst) (H: w ≤ b)
  : (t.subst_base b σ).wk1_base w = (t.wk1_base w).subst_base (b + 1) σ
  := by {
    have ⟨b', H⟩ := Nat.le.dest H;
    rw [Nat.add_comm] at H
    rw [<-H, subst_base_wk1]
  }
theorem Term.subst_base_wkn (t: Term) (b: Nat) (σ: Subst) (n: Nat)
  : (t.subst_base b σ).wkn_base b n = (t.wkn_base b n).subst_base (n + b) σ
  := by induction n generalizing t b σ with
     | zero => rw [wkn_base0, wkn_base0, Nat.zero_add]
     | succ n I =>
        simp only [wkn_base_wk1_base, I]
        generalize wkn_base t b n = t'
        rw [Nat.succ_add, subst_base_wk1]
        rfl
theorem Term.subst_wkn (t: Term) (σ: Subst) (n: Nat)
  : (t.subst σ).wkn n = (t.wkn n).subst_base n σ
  := t.subst_base_wkn 0 σ n

theorem Term.subst_base_comp (t: Term) (n: Nat) (σ τ: Subst)
  : t.subst_base n (σ.comp τ) = (t.subst_base n τ).subst_base n σ
  := by induction t generalizing n σ τ with
     | var v =>
      simp only [subst_base_var, Subst.comp, Subst.liftn]
      split
      simp [subst_base_var, Subst.liftn, Subst.to_term_fold, *]
      rw [subst_wkn]
     | _ => simp only [subst_base, fold_base, *]; rfl
theorem Term.subst_comp (t: Term) (σ τ: Subst): t.subst (σ.comp τ) = (t.subst τ).subst σ
  := t.subst_base_comp 0 σ τ

def Wk.to_subst (ρ: Wk): Term.Subst := λv => Term.var (ρ.app v)

theorem Wk.lift_to_subst (ρ: Wk):
  ρ.lift.to_subst = ρ.to_subst.lift
  := by funext n; cases n <;> rfl
theorem Wk.liftn_to_subst' (ρ: Wk) (n):
  (ρ.liftn n).to_subst = ρ.to_subst.liftn' n
  := by induction n generalizing ρ with
    | zero => rfl
    | succ => simp only [liftn, lift_to_subst, Term.Subst.liftn', *]
theorem Wk.liftn_to_subst (ρ: Wk) (n):
  (ρ.liftn n).to_subst = ρ.to_subst.liftn n
  := by rw [Term.Subst.liftn_eq_liftn', liftn_to_subst']
theorem Wk.to_subst_to_term_fold (ρ: Wk):
  ρ.to_subst.to_term_fold = ρ.to_term_fold
  := by {
    funext b _;
    simp only [to_term_fold, Term.Subst.to_term_fold, <-Wk.liftn_to_subst]
    rfl
  }
theorem Wk.to_subst_subst_base (ρ: Wk) (t: Term) (n)
  : t.subst_base n ρ.to_subst = t.wk_base n ρ
  := by induction t <;> simp only [Term.subst_base, Term.wk_base, to_subst_to_term_fold]
theorem Wk.to_subst_subst (ρ: Wk) (t: Term)
  : t.subst ρ.to_subst = t.wk ρ
  := ρ.to_subst_subst_base t 0

def Term.to_subst (t: Term): Subst
  := λv => match v with | 0 => t | v + 1 => var v

abbrev Term.substn (t: Term) (n: Nat) (u: Term) := t.subst_base n u.to_subst
abbrev Term.subst0 (t: Term) (u: Term): Term := t.substn 0 u

def Term.to_alpha (t: Term): Subst
  := λv => match v with | 0 => t | v => var v

theorem Term.to_alpha_char (t: Term): t.to_alpha = t.to_subst.comp (Wk.wknth 1).to_subst
  := by {
    funext v
    cases v with
    | zero => simp only [to_alpha, Subst.comp, Wk.to_subst, subst_var, to_subst, Wk.app_liftn]
    | succ v => simp_arith [
      to_alpha, Subst.comp, Wk.to_subst, subst_var, to_subst, Wk.app_liftn, Wk.app_wk1,
      Subst.to_term_fold, Subst.liftn, Wk.to_term_fold, Wk.app
    ]
  }

abbrev Term.alphan (t: Term) (n: Nat) (u: Term) := t.subst_base n u.to_alpha
abbrev Term.alpha0 (t: Term) (u: Term): Term := t.alphan 0 u

theorem Term.alphan_char (t: Term) (n: Nat) (u: Term): t.alphan n u = (t.wknth_base n 1).substn n u
  := by simp only [substn, wknth_base, <-Wk.to_subst_subst_base, <-subst_base_comp, <-to_alpha_char]
theorem Term.alpha0_char (t: Term) (u: Term): t.alpha0 u = (t.wknth 1).subst0 u
  := t.alphan_char 0 u

def Term.Subst.id: Subst := λx => var x

theorem Term.Subst.liftn_id (n: Nat): id.liftn n = id
  := by {
    funext v; simp only [liftn]; split; rfl
    simp only [id, Term.wkn, Term.wk_var, Wk.app_wkn,
      Nat.sub_add_cancel (Nat.le_of_not_lt (by assumption))]
  }
theorem Term.subst_base_id (t: Term) (n: Nat): t.subst_base n Subst.id = t
  := by induction t generalizing n with
     | var v => simp only [subst_base_var, Subst.liftn_id]; rfl
     | _ => simp only [subst_base, fold_base, *]; rfl

theorem Term.to_subst_wk1_id (t: Term): t.to_subst.comp Wk.wk1.to_subst = Subst.id
  := by funext v; cases v <;> rfl
theorem Term.substn_wk1_base (t: Term) (u: Term) (n: Nat): (t.wk1_base n).substn n u = t
  := by rw [
    wk1_base, <-Wk.to_subst_subst_base, substn, <-subst_base_comp, to_subst_wk1_id, subst_base_id]
theorem Term.subst0_wk1 (t: Term) (u: Term): t.wk1.subst0 u = t := t.substn_wk1_base u 0

theorem Term.to_alpha_wk1_wk1 (t: Term): t.to_alpha.comp Wk.wk1.to_subst = Wk.wk1.to_subst
  := by funext v; cases v <;> rfl
theorem Term.alphan_wk1_base (t: Term) (u: Term) (n: Nat): (t.wk1_base n).alphan n u = t.wk1_base n
  := by rw [
    wk1_base, <-Wk.to_subst_subst_base, alphan, <-subst_base_comp, to_alpha_wk1_wk1]
theorem Term.alpha0_wk1 (t: Term) (u: Term): t.wk1.alpha0 u = t.wk1 := t.alphan_wk1_base u 0
