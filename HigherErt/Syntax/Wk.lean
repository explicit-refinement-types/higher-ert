import HigherErt.VarUtils.Wk
import HigherErt.Syntax.Term

def Wk.to_term_fold (ρ: Wk) (b: Nat): Term.Shape Term -> Term
  | Term.Shape.var n => Term.var ((ρ.liftn b).app n)
  | t => t.to_term

theorem Wk.to_term_fold_injective (ρ: Wk) (b: Nat)
  : Function.Injective (ρ.to_term_fold b)
  := by {
    intro x y;
    cases x
    <;> cases y
    <;> simp [to_term_fold, Term.Shape.to_term]
    apply Wk.injective
  }

theorem to_term_fold_coherent: Wk.coherent Wk.to_term_fold
  := λH => by funext b _; rw [Wk.to_term_fold, H.liftn]; rfl

abbrev Term.wk_base (t: Term) (n: Nat) (ρ: Wk): Term := t.fold_base n ρ.to_term_fold

-- theorem Term.wk_base_injective (ρ: Wk) (b: Nat)
--   : Function.Injective (λt: Term => t.wk_base b ρ)
--   := by {
--     intro x y;
--     sorry
--   }

theorem Term.wk_base_var (v n: Nat) (ρ: Wk): (var v).wk_base n ρ = var ((ρ.liftn n).app v) := rfl

abbrev Wk.term_app_base (ρ: Wk) (n: Nat) (t: Term) := t.wk_base n ρ

theorem Term.wk_base_term_app_base (t: Term) (n: Nat) (ρ: Wk)
  : t.wk_base n ρ = ρ.term_app_base n t
  := rfl

@[aesop safe]
theorem wk_base_coherent (t: Term) (n: Nat): Wk.coherent (Term.wk_base t n)
  := λH => by simp only [Term.wk_base, to_term_fold_coherent H]

@[aesop safe]
theorem wk_term_app_base_coherent: Wk.coherent Wk.term_app_base
  := λH => by funext t n; exact wk_base_coherent n t H

abbrev Term.wk (t: Term) (ρ: Wk): Term:= t.wk_base 0 ρ
theorem Term.wk_var (v: Nat) (ρ: Wk): (var v).wk ρ = var (ρ.app v) := rfl
abbrev Wk.term_app (ρ: Wk) (t: Term) := t.wk ρ
theorem term_app_term_app_base_zero (ρ: Wk): ρ.term_app = ρ.term_app_base 0 := rfl

@[aesop safe]
theorem Term.wk_term_app (t: Term) (ρ: Wk)
  : t.wk ρ = ρ.term_app t
  := rfl

@[aesop safe]
theorem wk_coherent (t: Term): Wk.coherent t.wk := wk_base_coherent t 0

@[aesop unsafe 50%]
theorem wk_coherent_aesop_helper (t: Term) (ρ σ: Wk) (H: ρ.equiv σ)
  : t.wk ρ = t.wk σ
  := by rw [wk_coherent t H]

@[aesop safe]
theorem wk_term_app_coherent: Wk.coherent Wk.term_app
  := λH => by simp only [term_app_term_app_base_zero]; rw [wk_term_app_base_coherent H]

@[aesop safe]
theorem Term.wk_base_succ (t: Term) (n ρ): t.wk_base n.succ ρ = t.wk_base n ρ.lift
  := by induction t generalizing n ρ with
     | var => simp only [wk_base, fold_base, Wk.to_term_fold, Wk.lift_liftn]
     | _ => simp only [wk_base, fold_base, *]; rfl
@[aesop safe]
theorem Term.wk_base_char (t: Term) (n ρ): t.wk_base n ρ = t.wk (ρ.liftn n)
  := by induction n generalizing ρ with
    | zero => rfl
    | succ _ I => rw [wk_base_succ, I, Wk.lift_liftn]
@[aesop safe]
theorem Term.wk_base_id_liftn(t: Term) (n: Nat) (m: Nat): t.wk_base n (Wk.id.liftn m) = t
  := by induction t generalizing n m with
     | var =>
      simp only [wk_base, fold_base, Wk.to_term_fold, Wk.liftn_add]
      rw [Wk.id.equiv_refl.liftn_equiv_id]
      rfl
     | _ => simp only [wk_base, fold_base, *]; rfl
@[aesop safe, simp]
theorem Term.wk_id_liftn (t: Term) (n: Nat): t.wk (Wk.id.liftn n) = t := t.wk_base_id_liftn 0 n
@[aesop safe, simp]
theorem Term.wk_base_id (t: Term) (n: Nat): t.wk_base n Wk.id = t := t.wk_base_id_liftn n 0
@[aesop safe, simp]
theorem Term.wk_id (t: Term): t.wk Wk.id = t := t.wk_base_id 0

@[aesop safe, simp]
theorem Term.wk_base_comp (t: Term) (n: Nat) (ρ σ: Wk)
  : t.wk_base n (ρ.comp σ) = (t.wk_base n σ).wk_base n ρ
  := by induction t generalizing n ρ σ with
     | var => simp only [wk_base, fold_base, Wk.to_term_fold, Wk.liftn_dist_comp, Wk.app_comp]
     | _ => simp only [wk_base, fold_base, *]; rfl
@[aesop safe, simp]
theorem Term.wk_comp (t: Term) (ρ σ: Wk)
  : t.wk (ρ.comp σ) = (t.wk σ).wk ρ
  := t.wk_base_comp 0 ρ σ

abbrev Term.wkn_base (t: Term) (b: Nat) (n: Nat): Term := t.wk_base b (Wk.wkn n)
abbrev Term.wknth_base (t: Term) (b: Nat) (n: Nat): Term := t.wk_base b (Wk.wknth n)
abbrev Term.wk1_base (t: Term) (b: Nat): Term := t.wk_base b (Wk.wk1)
abbrev Term.wkn (t: Term) (n: Nat): Term := t.wk (Wk.wkn n)
abbrev Term.wknth (t: Term) (n: Nat): Term := t.wk (Wk.wknth n)
abbrev Term.wk1 (t: Term): Term := t.wk Wk.wk1

@[aesop safe, simp]
theorem Term.wkn_base0 (t: Term) (b): t.wkn_base b 0 = t := t.wk_base_id b
@[aesop safe, simp]
theorem Term.wkn_base1 (t: Term) (b): t.wkn_base b 1 = t.wk1_base b := rfl
@[aesop safe, simp]
theorem Term.wknth_base0 (t: Term) (b): t.wknth_base b 0 = t.wk1_base b := rfl
@[aesop safe, simp]
theorem Term.wkn0 (t: Term): t.wkn 0 = t := t.wk_id
@[aesop safe, simp]
theorem Term.wkn1 (t: Term): t.wkn 1 = t.wk1 := rfl
@[aesop safe, simp]
theorem Term.wknth0 (t: Term): t.wknth 0 = t.wk1 := rfl
@[aesop safe]
theorem Term.wk1_base_wknth (t: Term) (b): t.wk1_base b = t.wknth b
  := wk_base_char t b Wk.wk1
@[aesop safe]
theorem Term.wknth_base_wknth (t: Term) (n b): t.wknth_base n b = t.wknth (b + n)
  := by rw [wknth_base, wk_base_char, Wk.liftn_add]
@[aesop safe]
theorem Term.wkn_base_wk1_base (t: Term) (n b)
  : t.wkn_base b n.succ = (t.wkn_base b n).wk1_base b
  := by rw [wk1_base, wkn_base, <-t.wk_base_comp]; rfl
@[aesop safe, simp]
theorem Term.wkn_wk1 (t: Term) (n): t.wkn n.succ = (t.wkn n).wk1
  := by rw [wk1, wkn, <-t.wk_comp]; rfl
@[aesop safe, simp]
theorem Term.wk1_wkn (t: Term) (n): t.wkn n.succ = t.wk1.wkn n
  := by rw [wk1, wkn, wkn, <-t.wk_comp, Wk.wkn_comp_wk1];

@[aesop norm unfold]
abbrev Term.is_wk (A B: Term) (ρ: Wk): Prop := A = B.wk ρ
@[aesop unsafe 90%]
theorem Term.is_wk_refl_equiv_id (A: Term) (ρ: Wk) (H: ρ.equiv Wk.id)
  : A.wk ρ = A := by rw [wk_coherent A H, wk_id]
@[aesop unsafe 90%]
theorem Term.is_wk_refl_equiv_id_symm (A: Term) (ρ: Wk) (H: ρ.equiv Wk.id)
  : A = A.wk ρ := (is_wk_refl_equiv_id A ρ H).symm
