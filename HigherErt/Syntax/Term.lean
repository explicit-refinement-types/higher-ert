import Mathlib.Logic.Equiv.Defs
import Aesop

inductive Kind: Type
| ty
| pr
| gs
deriving Repr, DecidableEq

inductive Term: Type
-- Variables
| var (n: Nat)

-- Type and proposition formers
| pi (kA kB: Kind) (A B: Term)
| sigma (kA kB: Kind) (A B: Term)
| coprod (k: Kind) (A B: Term)

-- Term/proof formers
| let_ (k: Kind) (e e': Term)
| lam (k: Kind) (A t: Term)
| app (k: Kind) (s t: Term)
| pair (kl kr: Kind) (l r: Term)
| let_pair (kA kB: Kind) (e e': Term)
| inj (b: Fin 2) (e: Term)
| case (e l r: Term)
| abort (e: Term)

-- Base types
| unit
| prop

-- Base kinds
| type

-- Base propositions
| bot
| top

-- Base terms
| nil

-- Base proofs
| triv

-- Theory of equality
| eq (a b: Term)
| refl (a: Term)
| sbst (F a b: Term)
| reduce (a b: Term)
| irir (f a b: Term)
| prir (F a b: Term)
| unit_unique (a b: Term)
| discr (a b: Term)

-- Theory of natural numbers
| nat
| zero
| succ (n: Term)
| natrec (M e z s: Term)
deriving Repr, DecidableEq

inductive Term.Shape (α: Type): Type
-- Variables
| var (n: Nat)

-- Type and proposition formers
| pi (kA kB: Kind) (A B: α)
| sigma (kA kB: Kind) (A B: α)
| coprod (k: Kind) (A B: α)

-- Term/proof formers
| let_ (k: Kind) (e e': α)
| lam (k: Kind) (A t: α)
| app (k: Kind) (s t: α)
| pair (kl kr: Kind) (l r: α)
| let_pair (kA kB: Kind) (e e': α)
| inj (b: Fin 2) (e: α)
| case (e l r: α)
| abort (e: α)

-- Base types
| unit
| prop

-- Base kinds
| type

-- Base propositions
| bot
| top

-- Base terms
| nil

-- Base proofs
| triv

-- Theory of equality
| eq (a b: α)
| refl (a: α)
| sbst (F a b: α)
| reduce (a b: α)
| irir (f a b: α)
| prir (F a b: α)
| unit_unique (a b: α)
| discr (a b: α)

-- Theory of natural numbers
| nat
| zero
| succ (n: α)
| natrec (M e z s: α)
deriving Repr, DecidableEq

def Term.Shape.args {α}: Term.Shape α -> List α
  | pi _ _ A B => [A, B]
  | sigma _ _ A B => [A, B]
  | coprod _ A B => [A, B]
  | let_ _ e e' => [e, e']
  | lam _ A t => [A, t]
  | app _ s t => [s, t]
  | pair _ _ l r => [l, r]
  | let_pair _ _ e e' => [e, e']
  | inj _ e => [e]
  | case e l r => [e, l, r]
  | abort e => [e]
  | eq a b => [a, b]
  | refl a => [a]
  | sbst F a b => [F, a, b]
  | reduce a b => [a, b]
  | irir f a b => [f, a, b]
  | prir F a b => [F, a, b]
  | unit_unique a b => [a, b]
  | discr a b => [a, b]
  | succ n => [n]
  | natrec M e z s => [M, e, z, s]
  | _ => []

@[simp]
def Term.Shape.shifted_map {α β} (F: Nat -> α → β) (t: Term.Shape α) := match t with
    | Term.Shape.var n => Term.Shape.var n
    | Term.Shape.pi kA kB A B => Term.Shape.pi kA kB (F 0 A) (F 1 B)
    | Term.Shape.sigma kA kB A B => Term.Shape.sigma kA kB (F 0 A) (F 1 B)
    | Term.Shape.coprod k A B => Term.Shape.coprod k (F 0 A) (F 0 B)
    | Term.Shape.let_ k e e' => Term.Shape.let_ k (F 0 e) (F 1 e')
    | Term.Shape.lam k A t => Term.Shape.lam k (F 0 A) (F 1 t)
    | Term.Shape.app k s t => Term.Shape.app k (F 0 s) (F 0 t)
    | Term.Shape.pair kl kr l r => Term.Shape.pair kl kr (F 0 l) (F 0 r)
    | Term.Shape.let_pair kA kB e e' => Term.Shape.let_pair kA kB (F 0 e) (F 2 e')
    | Term.Shape.inj b e => Term.Shape.inj b (F 0 e)
    | Term.Shape.case e l r => Term.Shape.case (F 0 e) (F 1 l) (F 1 r)
    | Term.Shape.abort e => Term.Shape.abort (F 0 e)
    | Term.Shape.unit => Term.Shape.unit
    | Term.Shape.prop => Term.Shape.prop
    | Term.Shape.type => Term.Shape.type
    | Term.Shape.bot => Term.Shape.bot
    | Term.Shape.top => Term.Shape.top
    | Term.Shape.nil => Term.Shape.nil
    | Term.Shape.triv => Term.Shape.triv
    | Term.Shape.eq a b => Term.Shape.eq (F 0 a) (F 0 b)
    | Term.Shape.refl a => Term.Shape.refl (F 0 a)
    | Term.Shape.sbst F' a b => Term.Shape.sbst (F 1 F') (F 0 a) (F 0 b)
    | Term.Shape.reduce a b => Term.Shape.reduce (F 0 a) (F 0 b)
    | Term.Shape.irir f a b => Term.Shape.irir (F 0 f) (F 0 a) (F 0 b)
    | Term.Shape.prir F' a b => Term.Shape.prir (F 1 F') (F 0 a) (F 0 b)
    | Term.Shape.unit_unique a b => Term.Shape.unit_unique (F 0 a) (F 0 b)
    | Term.Shape.discr a b => Term.Shape.discr (F 0 a) (F 0 b)
    | Term.Shape.nat => Term.Shape.nat
    | Term.Shape.zero => Term.Shape.zero
    | Term.Shape.succ n => Term.Shape.succ (F 0 n)
    | Term.Shape.natrec M e z s => Term.Shape.natrec (F 1 M) (F 0 e) (F 0 z) (F 2 s)

instance Term.Shape_functor: Functor Term.Shape := {
  map := λF t => t.shifted_map (λ_ => F)
}

def Term.Shape.shifts {α}: Term.Shape α -> List Nat
  | pi _ _ _ _ => [0, 1]
  | sigma _ _ _ _ => [0, 1]
  | coprod _ _ _ => [0, 0]
  | let_ _ _ _ => [0, 1]
  | lam _ _ _ => [0, 1]
  | app _ _ _ => [0, 0]
  | pair _ _ _ _ => [0, 0]
  | let_pair _ _ _ _ => [0, 2]
  | inj _ _ => [0]
  | case _ _ _ => [0, 1, 1]
  | abort _ => [0]
  | eq _ _ => [0, 0]
  | refl _ => [0]
  | sbst _ _ _ => [1, 0, 0]
  | reduce _ _ => [0, 0]
  | irir _ _ _ => [0, 0, 0]
  | prir _ _ _ => [1, 0, 0]
  | unit_unique _ _ => [0, 0]
  | discr _ _ => [0, 0]
  | succ _ => [0]
  | natrec _ _ _ _ => [1, 0, 0, 2]
  | _ => []

abbrev Term.Shape.foldl_args {α β} (t: Term.Shape α): (β -> α -> β) -> β -> β
  := t.args.foldl

abbrev Term.Shape.foldr_args {α β} (t: Term.Shape α): (α -> β -> β) -> β -> β
  := t.args.foldr

@[simp]
def Term.Shape.to_term: Term.Shape Term -> Term
  | var n => Term.var n
  | pi kA kB A B => Term.pi kA kB A B
  | sigma kA kB A B => Term.sigma kA kB A B
  | coprod k A B => Term.coprod k A B
  | let_ k e e' => Term.let_ k e e'
  | lam k A t => Term.lam k A t
  | app k s t => Term.app k s t
  | pair kl kr l r => Term.pair kl kr l r
  | let_pair kA kB e e' => Term.let_pair kA kB e e'
  | inj b e => Term.inj b e
  | case e l r => Term.case e l r
  | abort e => Term.abort e
  | unit => Term.unit
  | prop => Term.prop
  | type => Term.type
  | bot => Term.bot
  | top => Term.top
  | nil => Term.nil
  | triv => Term.triv
  | eq a b => Term.eq a b
  | refl a => Term.refl a
  | sbst F a b => Term.sbst F a b
  | reduce a b => Term.reduce a b
  | irir f a b => Term.irir f a b
  | prir F a b => Term.prir F a b
  | unit_unique a b => Term.unit_unique a b
  | discr a b => Term.discr a b
  | nat => Term.nat
  | zero => Term.zero
  | succ n => Term.succ n
  | natrec M e z s => Term.natrec M e z s

@[simp]
def Term.to_fix: Term -> Term.Shape Term
  | var n => Term.Shape.var n
  | pi kA kB A B => Term.Shape.pi kA kB A B
  | sigma kA kB A B => Term.Shape.sigma kA kB A B
  | coprod k A B => Term.Shape.coprod k A B
  | let_ k e e' => Term.Shape.let_ k e e'
  | lam k A t => Term.Shape.lam k A t
  | app k s t => Term.Shape.app k s t
  | pair kl kr l r => Term.Shape.pair kl kr l r
  | let_pair kA kB e e' => Term.Shape.let_pair kA kB e e'
  | inj b e => Term.Shape.inj b e
  | case e l r => Term.Shape.case e l r
  | abort e => Term.Shape.abort e
  | unit => Term.Shape.unit
  | prop => Term.Shape.prop
  | type => Term.Shape.type
  | bot => Term.Shape.bot
  | top => Term.Shape.top
  | nil => Term.Shape.nil
  | triv => Term.Shape.triv
  | eq a b => Term.Shape.eq a b
  | refl a => Term.Shape.refl a
  | sbst F a b => Term.Shape.sbst F a b
  | reduce a b => Term.Shape.reduce a b
  | irir f a b => Term.Shape.irir f a b
  | prir F a b => Term.Shape.prir F a b
  | unit_unique a b => Term.Shape.unit_unique a b
  | discr a b => Term.Shape.discr a b
  | nat => Term.Shape.nat
  | zero => Term.Shape.zero
  | succ n => Term.Shape.succ n
  | natrec M e z s => Term.Shape.natrec M e z s

theorem term_to_Term.Shape_to_term (t: Term): t.to_fix.to_term = t :=
  by cases t <;> rfl

theorem Term.Shape_to_term_to_Term.Shape (t: Term.Shape Term): t.to_term.to_fix = t :=
  by cases t <;> rfl

def term_equiv_Term.Shape: Term ≃ Term.Shape Term := {
  toFun := Term.to_fix,
  invFun := Term.Shape.to_term,
  left_inv := term_to_Term.Shape_to_term,
  right_inv := Term.Shape_to_term_to_Term.Shape
}

@[simp]
def Term.fold_base {α} (t: Term) (base: Nat) (F: Nat -> Term.Shape α -> α): α :=
  let fixed := match t with
    | var n => Term.Shape.var n
    | pi kA kB A B => Term.Shape.pi kA kB (A.fold_base base F) (B.fold_base (base + 1) F)
    | sigma kA kB A B => Term.Shape.sigma kA kB (A.fold_base base F) (B.fold_base (base + 1) F)
    | coprod k A B => Term.Shape.coprod k (A.fold_base base F) (B.fold_base base F)
    | let_ k e e' => Term.Shape.let_ k (e.fold_base base F) (e'.fold_base (base + 1) F)
    | lam k A t => Term.Shape.lam k (A.fold_base base F) (t.fold_base (base + 1) F)
    | app k s t => Term.Shape.app k (s.fold_base base F) (t.fold_base base F)
    | pair kl kr l r => Term.Shape.pair kl kr (l.fold_base base F) (r.fold_base base F)
    | let_pair kA kB e e' => Term.Shape.let_pair kA kB (e.fold_base base F) (e'.fold_base (base + 2) F)
    | inj b e => Term.Shape.inj b (e.fold_base base F)
    | case e l r =>
      Term.Shape.case (e.fold_base base F) (l.fold_base (base + 1) F) (r.fold_base (base + 1) F)
    | abort e => Term.Shape.abort (e.fold_base base F)
    | unit => Term.Shape.unit
    | prop => Term.Shape.prop
    | type => Term.Shape.type
    | bot => Term.Shape.bot
    | top => Term.Shape.top
    | nil => Term.Shape.nil
    | triv => Term.Shape.triv
    | eq a b => Term.Shape.eq (a.fold_base base F) (b.fold_base base F)
    | refl a => Term.Shape.refl (a.fold_base base F)
    | sbst F' a b => Term.Shape.sbst
      (F'.fold_base (base + 1) F) (a.fold_base base F) (b.fold_base base F)
    | reduce a b => Term.Shape.reduce (a.fold_base base F) (b.fold_base base F)
    | irir f a b => Term.Shape.irir (f.fold_base base F) (a.fold_base base F) (b.fold_base base F)
    | prir F' a b => Term.Shape.prir (F'.fold_base (base + 1) F) (a.fold_base base F) (b.fold_base base F)
    | unit_unique a b => Term.Shape.unit_unique (a.fold_base base F) (b.fold_base base F)
    | discr a b => Term.Shape.discr (a.fold_base base F) (b.fold_base base F)
    | nat => Term.Shape.nat
    | zero => Term.Shape.zero
    | succ n => Term.Shape.succ (n.fold_base base F)
    | natrec M e z s =>
      Term.Shape.natrec (M.fold_base (base + 1) F)
        (e.fold_base base F) (z.fold_base base F) (s.fold_base (base + 2) F)
  F base fixed

theorem Term.fold_base_fixed
  (t: Term) (base: Nat) (F: Nat -> Term.Shape α -> α)
  : t.fold_base base F = F base (t.to_fix.shifted_map (λn t=> t.fold_base (base + n) F))
  := by cases t <;> rfl

-- theorem Term.fold_base_inj_fix {α}
--   (F: Nat -> Term -> Term.Shape α -> α)
--   (H: ∀n t, Function.Injective (F n t))
--   : ∀b, Function.Injective (λt: Term => t.fold_base b F)
--   := sorry

abbrev Term.fold {α} (t: Term) (F: Nat -> Term.Shape α -> α): α := t.fold_base 0 F

theorem Term.fold_fixed
  (t: Term) (F: Nat -> Term.Shape α -> α)
  : t.fold F = F 0 (t.to_fix.shifted_map (λn t=> t.fold_base n F))
  := fold_base_fixed t 0 F

abbrev Term.fold_float {α} (t: Term) (F: Term.Shape α → α): α := t.fold_base 0 (λ_=> F)

def Term.fv (t: Term): Nat := t.fold (λb t =>
  match t with | Term.Shape.var v => v.succ - b | t => t.foldl_args Nat.max 0)

def Term.has_var (t: Term) (n: Nat): Prop := t.fold (λb t =>
  match t with | Term.Shape.var v => v = n + b | t => t.foldl_args Or False)

@[simp]
abbrev Term.Shape.shape {α} (t: Term.Shape α): Term.Shape Unit := t.shifted_map (λ_ _ => ())
abbrev Term.Shape.shape_eq {α β} (l: Term.Shape α) (r: Term.Shape β): Prop := l.shape = r.shape

@[simp]
def Term.Shape.zip {α β} (l: Term.Shape α) (r: Term.Shape β): Option (Term.Shape (α × β))
  := match l, r with
    | var nl, var nr =>
      if nl = nr then some (Term.Shape.var nl) else none
    | pi kAl kBl Al Bl, pi kAr kBr Ar Br =>
      if kAl = kAr ∧ kBl = kBr then some (Term.Shape.pi kAl kBl (Al, Ar) (Bl, Br)) else none
    | sigma kAl kBl Al Bl, sigma kAr kBr Ar Br =>
      if kAl = kAr ∧ kBl = kBr then some (Term.Shape.sigma kAl kBl (Al, Ar) (Bl, Br)) else none
    | coprod kl Al Bl, coprod kr Ar Br =>
      if kl = kr then some (Term.Shape.coprod kl (Al, Ar) (Bl, Br)) else none
    | let_ kl el e'l, let_ kr er e'r =>
      if kl = kr then some (Term.Shape.let_ kl (el, er) (e'l, e'r)) else none
    | lam kl Al sl, lam kr Ar sr =>
      if kl = kr then some (Term.Shape.lam kl (Al, Ar) (sl, sr)) else none
    | app kl Al sl, app kr Ar sr =>
      if kl = kr then some (Term.Shape.app kl (Al, Ar) (sl, sr)) else none
    | pair kll krl ll rl, pair klr krr lr rr =>
      if kll = klr ∧ krl = krr then some (Term.Shape.pair kll krl (ll, lr) (rl, rr)) else none
    | let_pair kAl kBl el e'l, let_pair kAr kBr er e'r =>
      if kAl = kAr ∧ kBl = kBr then some (Term.Shape.let_pair kAl kBl (el, er) (e'l, e'r)) else none
    | inj bl el, inj br er =>
      if bl = br then some (Term.Shape.inj bl (el, er)) else none
    | case el ll rl, case er lr rr => some (Term.Shape.case (el, er) (ll, lr) (rl, rr))
    | abort el, abort er => some (Term.Shape.abort (el, er))
    | unit, unit => some Term.Shape.unit
    | prop, prop => some Term.Shape.prop
    | type, type => some Term.Shape.type
    | bot, bot => some Term.Shape.bot
    | top, top => some Term.Shape.top
    | nil, nil => some Term.Shape.nil
    | triv, triv => some Term.Shape.triv
    | eq al bl, eq ar br => some (Term.Shape.eq (al, ar) (bl, br))
    | refl al, refl ar => some (Term.Shape.refl (al, ar))
    | sbst Fl al bl, sbst Fr ar br => some (Term.Shape.sbst (Fl, Fr) (al, ar) (bl, br))
    | reduce al bl, reduce ar br => some (Term.Shape.reduce (al, ar) (bl, br))
    | irir fl al bl, irir fr ar br => some (Term.Shape.irir (fl, fr) (al, ar) (bl, br))
    | prir Fl al bl, prir Fr ar br => some (Term.Shape.prir (Fl, Fr) (al, ar) (bl, br))
    | unit_unique al bl, unit_unique ar br => some (Term.Shape.unit_unique (al, ar) (bl, br))
    | discr al bl, discr ar br => some (Term.Shape.unit_unique (al, ar) (bl, br))
    | nat, nat => some (Term.Shape.nat)
    | zero, zero => some (Term.Shape.zero)
    | succ nl, succ nr => some (Term.Shape.succ (nl, nr))
    | natrec Ml el zl sl, natrec Mr er zr sr =>
      some (Term.Shape.natrec (Ml, Mr) (el, er) (zl, zr) (sl, sr))
    | _, _ => none

def Term.Shape.zip_maximal {α β} (l: Term.Shape α) (r: Term.Shape β)
  : l.shape = r.shape -> l.zip r ≠ none
  := by cases l <;> simp only [shape, shifted_map] <;> cases r <;> simp [shape, shifted_map, zip]

def Term.Shape.zip_conservative {α β} (l: Term.Shape α) (r: Term.Shape β)
  : l.zip r ≠ none -> l.shape = r.shape
  := by cases l <;> simp only [shape, shifted_map] <;> cases r <;> simp [shape, shifted_map, zip]

def Term.Shape.zip_correct {α β} (l: Term.Shape α) (r: Term.Shape β)
  : l.shape = r.shape ↔ l.zip r ≠ none
  := ⟨l.zip_maximal r, l.zip_conservative r⟩

inductive Term.Shape.Dep {α: Type} (T: α → Term.Shape α → Type): Term.Shape α → Type
-- Variables
| var (n: Nat): (Term.Shape.var n).Dep T

-- Type and proposition formers
| pi (kA kB: Kind) {A B: α}: T A (Term.Shape.pi kA kB A B) -> T B (Term.Shape.pi kA kB A B) -> (Term.Shape.pi kA kB A B).Dep T
| sigma (kA kB: Kind) {A B: α}: T A (Term.Shape.sigma kA kB A B) -> T B (Term.Shape.sigma kA kB A B) -> (Term.Shape.sigma kA kB A B).Dep T
| coprod (k: Kind) {A B: α}: T A (Term.Shape.coprod k A B) -> T B (Term.Shape.coprod k A B) -> (Term.Shape.coprod k A B).Dep T

-- Term/proof formers
| let_ (k: Kind) {e e': α}: T e (Term.Shape.let_ k e e') -> T e' (Term.Shape.let_ k e e') -> (Term.Shape.let_ k e e').Dep T
| lam (k: Kind) {A t: α}: T A (Term.Shape.lam k A t) -> T t (Term.Shape.lam k A t) -> (Term.Shape.lam k A t).Dep T
| app (k: Kind) {s t: α}: T s (Term.Shape.app k s t) -> T t (Term.Shape.app k s t) -> (Term.Shape.app k s t).Dep T
| pair (kl kr: Kind) {l r: α}: T l (Term.Shape.pair kl kr l r) -> T r (Term.Shape.pair kl kr l r) -> (Term.Shape.pair kl kr l r).Dep T
| let_pair (kA kB: Kind) {e e': α}: T e (Term.Shape.let_pair kA kB e e') -> T e' (Term.Shape.let_pair kA kB e e') -> (Term.Shape.let_pair kA kB e e').Dep T
| inj (b: Fin 2) {e: α}: T e (Term.Shape.inj b e) -> (Term.Shape.inj b e).Dep T
| case {e l r: α}: T e (Term.Shape.case e l r) -> T l (Term.Shape.case e l r) -> T r (Term.Shape.case e l r) -> (Term.Shape.case e l r).Dep T
| abort {e: α}: T e (Term.Shape.abort e) -> (Term.Shape.abort e).Dep T

-- Base types
| unit: Term.Shape.unit.Dep T
| prop: Term.Shape.prop.Dep T

-- Base kinds
| type: Term.Shape.type.Dep T

-- Base propositions
| bot: Term.Shape.bot.Dep T
| top: Term.Shape.top.Dep T

-- Base terms
| nil: Term.Shape.nil.Dep T

-- Base proofs
| triv: Term.Shape.triv.Dep T

-- Theory of equality
| eq {a b: α}: T a (Term.Shape.eq a b) -> T b (Term.Shape.eq a b) -> (Term.Shape.eq a b).Dep T
| refl {a: α}: T a (Term.Shape.refl a) -> (Term.Shape.refl a).Dep T
| sbst {F a b: α}: T F (Term.Shape.sbst F a b) -> T a (Term.Shape.sbst F a b) -> T b (Term.Shape.sbst F a b) -> (Term.Shape.sbst F a b).Dep T
| reduce {a b: α}: T a (Term.Shape.reduce a b) -> T b (Term.Shape.reduce a b) -> (Term.Shape.reduce a b).Dep T
| irir {f a b: α}: T f (Term.Shape.irir f a b) -> T a (Term.Shape.irir f a b) -> T b (Term.Shape.irir f a b) -> (Term.Shape.irir f a b).Dep T
| prir {F a b: α}: T F (Term.Shape.prir F a b) -> T a (Term.Shape.prir F a b) -> T b (Term.Shape.prir F a b) -> (Term.Shape.prir F a b).Dep T
| unit_unique {a b: α}: T a (Term.Shape.unit_unique a b) -> T b (Term.Shape.unit_unique a b) -> (Term.Shape.unit_unique a b).Dep T
| discr {a b: α}: T a (Term.Shape.discr a b) -> T b (Term.Shape.discr a b) -> (Term.Shape.discr a b).Dep T

-- Theory of natural numbers
| nat: Term.Shape.nat.Dep T
| zero: Term.Shape.zero.Dep T
| succ {n: α}: T n (Term.Shape.succ n) -> (Term.Shape.succ n).Dep T
| natrec {M e z s: α}: T M (Term.Shape.natrec M e z s) -> T e (Term.Shape.natrec M e z s) -> T z (Term.Shape.natrec M e z s) -> T s (Term.Shape.natrec M e z s) -> (Term.Shape.natrec M e z s).Dep T
