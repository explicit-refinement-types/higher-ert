import HigherErt.Syntax.Term

abbrev Term.Shape.lift_rel {α} (R: α → α → Prop) (t t': Term.Shape α): Prop
  := match t.zip t' with
  | some t => t.foldl_args (λP (a, b) => P ∨ (R a b)) False
  | none => False

abbrev Term.Shape.lift_rel' {α} (R: α → α → Prop) (t t': Term.Shape α): Prop
  := t.shape = t'.shape ∧ ∃i: Nat, ∃h1 h2, R (t.args[i]'h1) (t.args[i]'h2)

-- theorem TFix.lift_rel_equiv (R: α → α → Prop) (t t': TFix α): t.lift_rel R t' ↔ t.lift_rel' R t'
--   := sorry

def Term.lift_rel_steps (R: Term -> Term -> Prop): Nat -> Term -> Term -> Prop
  | 0 => R
  | n + 1 => λs t => s.to_fix.lift_rel (lift_rel_steps R n) t.to_fix

def Term.lift_rel (R: Term -> Term -> Prop) (s t: Term): Prop
  := ∃n: ℕ, s.lift_rel_steps R n t
