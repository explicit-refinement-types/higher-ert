inductive Stlc.Ty
| empty
| unit
| fn (A B: Ty)
| nat
| prod (A B: Ty)
| coprod (A B: Ty)
deriving Repr, DecidableEq

inductive Stlc
| var (n: Nat)
| let_ (e e': Stlc)
| lam (A: Stlc.Ty) (t: Stlc)
| app (s t: Stlc)
| pair (l r: Stlc)
| let_pair (e e': Stlc)
| inj (b: Fin 2) (e: Stlc)
| cases (e l r: Stlc)
| abort (A: Stlc.Ty)
deriving Repr, DecidableEq

inductive Stlc.Fixed (α: Type)
| var (n: Nat)
| let_ (e e': α)
| lam (A: Stlc.Ty) (t: α)
| app (s t: α)
| pair (l r: α)
| let_pair (e e': α)
| inj (b: Fin 2) (e: α)
| cases (e l r: α)
| abort (A: Stlc.Ty)
deriving Repr, DecidableEq
