import HigherErt.Syntax.Term
import HigherErt.Syntax.Wk
import HigherErt.EqUtils.Basic
import Aesop

--TODO: keep metavariable-producing rules out of the global goal-set

open Kind

def Kind.upgrade: Kind -> Kind
| gs => ty
| k => k

@[simp] theorem Kind.upgrade_idem (k: Kind): k.upgrade.upgrade = k.upgrade := by cases k <;> rfl

@[aesop [unsafe 50% cases, safe constructors]]
inductive Kind.is_upgrade: Kind -> Kind -> Prop
| refl (k: Kind): k.is_upgrade k
| gs: ty.is_upgrade gs

@[aesop [unsafe 50% cases, safe constructors]]
inductive Kind.is_upgrade_t: Kind -> Kind -> Type
| refl (k: Kind): k.is_upgrade_t k
| gs: ty.is_upgrade_t gs

@[aesop norm]
def Kind.is_upgrade_t.coherent {k k': Kind} (H H': k.is_upgrade_t k'): H = H'
  := by cases H <;> cases H' <;> rfl

--TODO: think about these...
@[aesop unsafe 90%]
def Kind.is_upgrade.to_ind {k k': Kind} (H: k.is_upgrade k'): k.is_upgrade_t k'
  := by cases k <;> cases k' <;> first | constructor | exact False.elim (match H with .)
@[aesop unsafe 50%]
def Kind.is_upgrade_t.to_prop {k k': Kind} (H: k.is_upgrade_t k'): k.is_upgrade k'
  := by cases H <;> constructor

@[aesop safe]
abbrev Kind.is_upgrade_refl: (k: Kind) -> k.is_upgrade k := Kind.is_upgrade.refl
@[aesop safe]
abbrev Kind.is_upgrade_t_refl: (k: Kind) -> k.is_upgrade_t k := Kind.is_upgrade_t.refl
@[aesop unsafe 50%]
abbrev Kind.is_upgrade_eq {k k': Kind} (p: k = k'): k.is_upgrade k' := p ▸ k.is_upgrade_refl
abbrev Kind.is_upgrade_t_eq {k k': Kind} (p: k = k'): k.is_upgrade_t k' := p ▸ k.is_upgrade_t_refl

@[aesop unsafe 50%]
theorem Kind.is_upgrade.trans {k1 k2 k3: Kind} (H: k1.is_upgrade k2) (H': k2.is_upgrade k3)
  : k1.is_upgrade k3
  := by cases H <;> cases H' <;> constructor
@[aesop unsafe 50%]
theorem Kind.Ctx.is_upgrade.antisymm {k1 k2: Kind} (H: k1.is_upgrade k2) (H': k2.is_upgrade k1): k1 = k2
  := by cases H <;> cases H'; rfl
@[aesop safe]
theorem Kind.upgrade_is_upgrade (k: Kind): k.upgrade.is_upgrade k
  := by cases k <;> constructor
def Kind.is_upgrade_t.trans {k1 k2 k3: Kind} (H: k1.is_upgrade k2) (H': k2.is_upgrade k3)
  : k1.is_upgrade k3
  := by cases H <;> cases H' <;> constructor
def Kind.Ctx.is_upgrade_t.antisymm {k1 k2: Kind} (H: k1.is_upgrade k2) (H': k2.is_upgrade k1): k1 = k2
  := by cases H <;> cases H'; rfl
@[aesop safe]
def Kind.upgrade_is_upgrade_t (k: Kind): k.upgrade.is_upgrade_t k
  := k.upgrade_is_upgrade.to_ind

@[aesop [unsafe 50% cases, safe constructors]]
inductive Kind.var_kind: Kind -> Term -> Type
| pr: pr.var_kind prop
| ty: ty.var_kind type

@[aesop [unsafe 50% cases, safe constructors]]
inductive Kind.up_kind: Kind -> Term -> Type
| up: pr.up_kind prop
| ty: ty.up_kind type
| gs: gs.up_kind type

@[aesop unsafe 50% [cases]]
structure Hyp :=
  kind: Kind
  hyp: Term

@[aesop unsafe 50% [constructors, cases]]
structure Hyp.is_upgrade (H H': Hyp): Prop :=
  kind_up: H.kind.is_upgrade H'.kind
  hyp_eq: H.hyp = H'.hyp

@[aesop safe]
def Hyp.is_upgrade_refl (H: Hyp): H.is_upgrade H := by apply Hyp.is_upgrade.mk <;> constructor
@[aesop unsafe 50%]
theorem Hyp.is_upgrade.trans {H1 H2 H3: Hyp}
  : H1.is_upgrade H2 -> H2.is_upgrade H3 -> H1.is_upgrade H3
  | ⟨k1, h1⟩, ⟨k2, h2⟩ => ⟨k1.trans k2, h1.trans h2⟩
@[aesop unsafe 50%]
theorem Hyp.is_upgrade.antisymm {H1 H2: Hyp} (H: H1.is_upgrade H2) (H': H2.is_upgrade H1)
  : H1 = H2
  := by aesop

instance hyp_upgrade_partial_ord: PartialOrder Hyp := {
  le := Hyp.is_upgrade,
  le_refl := Hyp.is_upgrade_refl,
  le_trans := @Hyp.is_upgrade.trans,
  le_antisymm := @Hyp.is_upgrade.antisymm
}

@[aesop [unsafe 50% cases, safe constructors]]
inductive Hyp.has_var: Hyp -> Kind -> Term -> Prop
| zero_ty (A: Term): has_var { kind := ty, hyp := A } Kind.ty A
| zero_pr (φ: Term): has_var { kind := pr, hyp := φ } Kind.pr φ
| zero_gs (A: Term): has_var { kind := gs, hyp := A } Kind.gs A

abbrev Hyp.upgrade (H: Hyp): Hyp := { H with kind := H.kind.upgrade }

inductive Term.Ctx
| nil
| cons (H: Hyp) (Γ: Ctx)

abbrev Term.Ctx.cons_ty (A: Term) (Γ: Ctx) := Γ.cons { kind := ty, hyp := A }
abbrev Term.Ctx.cons_pr (φ: Term) (Γ: Ctx) := Γ.cons { kind := pr, hyp := φ }
abbrev Term.Ctx.cons_gs (A: Term) (Γ: Ctx) := Γ.cons { kind := gs, hyp := A }
abbrev Term.Ctx.cons_kind (k: Kind) (A: Term) (Γ: Ctx) := Γ.cons { kind := k, hyp := A }

-- inductive Term.Ctx.AnyHypP (F: Hyp -> Sort): Ctx -> Prop
-- | zero (Γ: Ctx) (H: Hyp) (I: F H): AnyHypP F (Γ.cons H)
-- | succ (Γ: Ctx) (H: Hyp): AnyHypP F Γ -> AnyHypP F (Γ.cons H)

-- inductive Term.Ctx.AnyHypT (F: Hyp -> Sort): Ctx -> Type
-- | zero (Γ: Ctx) (H: Hyp) (I: F H): AnyHypT F (Γ.cons H)
-- | succ (Γ: Ctx) (H: Hyp): AnyHypT F Γ -> AnyHypT F (Γ.cons H)

-- inductive Term.Ctx.AllHypP (F: Hyp -> Sort): Ctx -> Prop
-- | nil: AllHypP F nil
-- | cons (Γ: Ctx) (H: Hyp) (n: Nat): F H -> AllHypP F Γ -> AllHypP F (Γ.cons H)

-- inductive Term.Ctx.AllHypT (F: Hyp -> Sort): Ctx -> Type
-- | nil: AllHypT F nil
-- | cons (Γ: Ctx) (H: Hyp) (n: Nat): F H -> AllHypT F Γ -> AllHypT F (Γ.cons H)

-- inductive Term.Ctx.FAnyHypP (F: Nat -> Hyp -> Sort): Ctx -> Nat -> Prop
-- | zero (Γ: Ctx) (H: Hyp) (n: Nat) (I: F n H): FAnyHypP F (Γ.cons H) n
-- | succ (Γ: Ctx) (H: Hyp) (n: Nat): FAnyHypP F Γ n -> FAnyHypP F (Γ.cons H) (n + 1)

-- inductive Term.Ctx.FAnyHypT (F: Nat -> Hyp -> Sort): Ctx -> Nat -> Type
-- | zero (Γ: Ctx) (H: Hyp) (n: Nat) (I: F n H): FAnyHypT F (Γ.cons H) n
-- | succ (Γ: Ctx) (H: Hyp) (n: Nat): FAnyHypT F Γ n -> FAnyHypT F (Γ.cons H) (n + 1)

-- TODO: report potential lean 4 bug, moving on...
-- theorem Term.Ctx.has_var_char (Γ: Ctx) (n k A):
--   Γ.has_var_p n k A ↔
--   Γ.FAnyHypP (λi H => i = 0 ∧ H.has_var_p k (A.wkn n)) n :=
--   by {
--     apply Iff.intro;
--     {
--       intro H;
--       induction H with
--       | zero => sorry
--       | succ => sorry
--     }
--     {
--       intro H;
--       induction H with
--       | zero => sorry
--       | succ => sorry
--     }
--   }

-- @[aesop unsafe 50% [cases, constructors]]
-- inductive Term.Ctx.has_var_ix: Ctx -> Nat -> Kind -> Term -> Type
-- | zero {Γ: Ctx} {H k}:
--   H.has_var k A -> (Γ.cons H).has_var_ix 0 k A
-- | succ {Γ: Ctx} {n k A H}:
--   Γ.has_var_ix n k A -> (Γ.cons H).has_var_ix (n + 1) k A.wk1

@[aesop unsafe 50% [cases, constructors]]
inductive Term.Ctx.has_var_raw: Ctx -> Nat -> Kind -> Term -> Prop
| zero {Γ: Ctx} {H k}:
  H.has_var k A -> (Γ.cons H).has_var_raw 0 k A
| succ {Γ: Ctx} {n k A H}:
  Γ.has_var_raw n k A -> (Γ.cons H).has_var_raw (n + 1) k A

-- @[aesop unsafe 50% [cases, constructors]]
-- inductive Term.Ctx.has_var_ix_t: Ctx -> Nat -> Kind -> Term -> Type
-- | zero {Γ: Ctx} {H k}:
--   H.has_var k A -> (Γ.cons H).has_var_ix_t 0 k A
-- | succ {Γ: Ctx} {n k A H}:
--   Γ.has_var_ix_t n k A -> (Γ.cons H).has_var_ix_t (n + 1) k A.wk1
inductive Term.Ctx.has_var_raw_t: Ctx -> Nat -> Kind -> Term -> Type
| zero {Γ: Ctx} {H k}:
  H.has_var k A -> (Γ.cons H).has_var_raw_t 0 k A
| succ {Γ: Ctx} {n k A H}:
  Γ.has_var_raw_t n k A -> (Γ.cons H).has_var_raw_t (n + 1) k A

--TODO: think about this
@[aesop safe]
def Term.Ctx.has_var_raw.to_ind {Γ: Ctx} {n k A} (H: Γ.has_var_raw n k A)
  : Γ.has_var_raw_t n k A
  := match n, Γ with
  | _, nil => False.elim (match H with .)
  | 0, cons _ Γ => has_var_raw_t.zero (by cases H; assumption)
  | n + 1, cons _ Γ => has_var_raw_t.succ (@to_ind Γ _ _ _ (by cases H; assumption))
--@[aesop unsafe 50%]
def Term.Ctx.has_var_raw_t.to_prop {Γ: Ctx} {n k A}: Γ.has_var_raw_t n k A -> Γ.has_var_raw n k A
  | zero H => has_var_raw.zero H
  | succ H => has_var_raw.succ H.to_prop

@[aesop safe]
theorem Term.Ctx.has_var_raw_t.coherent
  {Γ: Ctx} {n k A} (H H': Γ.has_var_raw_t n k A): H = H'
  := by induction H with
  | zero => cases H'; rfl
  | succ _ I => cases H'; apply congr rfl (I _)

@[aesop unsafe 50%]
theorem Term.Ctx.has_var_raw.unique
  {Γ: Ctx} {n k k' A B}
  (H: Γ.has_var_raw n k A) (H': Γ.has_var_raw n k' B): k = k' ∧ A = B
  := by induction H with
  | zero H => cases H' with | zero H' => cases H <;> cases H' <;> simp
  | succ _ I => cases H' with | succ H' => exact I H'

@[aesop unsafe 50%]
theorem Term.Ctx.has_var_raw.unique_kind
  {Γ: Ctx} {n k k' A}
  (H: Γ.has_var_raw n k A) (H': Γ.has_var_raw n k' A): k = k'
  := (H.unique H').left

@[aesop unsafe 50%]
theorem Term.Ctx.has_var_raw.unique_ty
  {Γ: Ctx} {n k A B}
  (H: Γ.has_var_raw n k A) (H': Γ.has_var_raw n k B): A = B
  := (H.unique H').right

structure Term.Ctx.has_var_t (Γ: Ctx) (n: Nat) (k: Kind) (A: Term) :=
  var_ty: Term
  raw: Γ.has_var_raw n k var_ty
  is_wkn: A.is_wk var_ty (Wk.wkn n)

@[aesop norm]
theorem Term.Ctx.has_var_t.coherent {Γ: Ctx} {n k A}
  (H H': Γ.has_var_t n k A): H = H'
  := by {
    have ⟨_, Ht⟩ := H.raw.unique H'.raw;
    cases H; cases H';
    simp at Ht;
    simp [Ht]
  }

@[aesop safe]
def Term.Ctx.has_var (Γ: Ctx) (n: Nat) (k: Kind) (A: Term) := Nonempty (Γ.has_var_t n k A)


-- def Term.Ctx.has_var_raw.cook_ix {Γ: Ctx} {n k A}: Γ.has_var_raw n k A -> Γ.has_var_ix n k (A.wkn n)
-- | zero H => A.wkn0.symm ▸ Ctx.has_var_ix.zero H
-- | succ H => A.wkn_wk1 _ ▸ Ctx.has_var_ix.succ H.cook_ix

-- def Term.Ctx.has_var_raw.cook {Γ: Ctx} {n k A} (H: Γ.has_var_raw n k A): Γ.has_var n k (A.wkn n)
-- := { var_ty := A, raw := H, is_wkn := rfl }

def Term.Ctx.has_var_t.to_zero {Γ: Ctx} {k A}
  (H: Γ.has_var_t 0 k A): Γ.has_var_raw 0 k A
  := H.is_wkn.trans H.var_ty.wkn0 ▸ H.raw

-- @[aesop unsafe 50%]
-- def Term.Ctx.has_var.to_zero {Γ: Ctx} {k A}
--   (H: Γ.has_var 0 k A): Γ.has_var_raw 0 k A
--   := let ⟨H⟩ := H; H.to_zero

-- @[aesop unsafe 50%]
-- def Term.Ctx.has_var_raw.to_zero {Γ: Ctx} {k A}
--   (H: Γ.has_var_raw 0 k A): Γ.has_var 0 k A
--   := { var_ty := A, raw := H, is_wkn := A.wkn0.symm }

-- @[aesop unsafe 50%]
-- def Term.Ctx.has_var.zero {H: Hyp} {Γ: Ctx} {k A}
--   (V: H.has_var k A):
--   (Γ.cons H).has_var 0 k A
--   := (Term.Ctx.has_var_raw.zero V).to_zero

-- @[aesop unsafe 50%]
-- def Term.Ctx.has_var_zero_raw (Γ: Ctx) (k: Kind) (A: Term):
--   Γ.has_var 0 k A ≃ Γ.has_var_raw 0 k A
--   := {
--     toFun := Term.Ctx.has_var.to_zero,
--     invFun := Term.Ctx.has_var_raw.to_zero,
--     left_inv := λ_ => has_var.coherent _ _,
--     right_inv := λ_ => rfl
--   }

-- TODO: need to prove weakening injective...
-- def Term.Ctx.has_var.raw' {Γ: Ctx} {n k A} (H: Γ.has_var n k (A.wkn n)): Γ.has_var_raw n k A
--   := sorry

-- def Term.Ctx.has_var_raw_equiv (Γ: Ctx) (n: Nat) (k: Kind) (A: Term):
--   Γ.has_var n k (A.wkn n) ≃ Γ.has_var_raw n k A
--   := {
--     toFun := Term.Ctx.has_var.raw',
--     invFun := Term.Ctx.has_var_raw.cook,
--     left_inv := sorry,
--     right_inv := sorry
--   }

-- def Term.Ctx.has_var.succ
--   {Γ: Ctx} {n k A H} (P: Γ.has_var n k A)
--   : (Γ.cons H).has_var n.succ k A.wk1
--   := {
--     var_ty := P.var_ty,
--     raw := P.raw.succ,
--     is_wkn := by {
--       simp only [is_wk, Term.wkn_wk1, wkn]
--       rw [<-P.is_wkn]
--     }
--   }

-- def Term.Ctx.has_var_ix.to_has_var {Γ: Ctx} {n k A}
--   : Γ.has_var_ix n k A -> Γ.has_var n k A
--   | has_var_ix.zero H => has_var.zero H
--   | has_var_ix.succ H => has_var.succ H.to_has_var

-- TODO: need to prove weakening injective...
-- def Term.Ctx.has_var.pred {Γ: Ctx} {n k A H}
--   (S: (Γ.cons H).has_var n.succ k A.wk1)
--   : Γ.has_var n k A
--   := {
--     var_ty := S.var_ty,
--     raw := match S.raw with | has_var_raw.succ H => H,
--     is_wkn := by {
--       simp only [is_wk]
--     }
--   }

-- def Term.Ctx.has_var_succ (Γ: Ctx) (k: Kind) (n: Nat) (A: Term):
--   Γ.has_var n k A ≃ (Γ.cons H).has_var n.succ k A.wk1
--   := {
--     toFun := Term.Ctx.has_var.succ,
--     invFun := Term.Ctx.has_var.pred,
--     left_inv := λ_ => has_var.coherent _ _,
--     right_inv := λ_ => has_var.coherent _ _,
--   }

-- TODO: need to prove weakening injective...
-- theorem Term.Ctx.has_var_ix.to_raw_helper {Γ: Ctx} {n k A B}
-- : Γ.has_var_ix n k A -> A = B.wkn n -> Γ.has_var_raw n k B
-- | zero H, p => (p.trans B.wkn0) ▸ Ctx.has_var_raw.zero H
-- | succ H, p => Ctx.has_var_raw.succ sorry

-- theorem Term.Ctx.has_var_ix.to_raw {Γ: Ctx} {n k A} (H: Γ.has_var_ix n k (A.wkn n))
--   : Γ.has_var_raw n k A
--   := H.to_raw_helper rfl

@[aesop [unsafe 50% cases, safe constructors]]
inductive Term.Ctx.is_upgrade: Ctx -> Ctx -> Prop
| nil: nil.is_upgrade nil
| cons (Γ Δ: Ctx) (H H': Hyp): H.is_upgrade H' -> Γ.is_upgrade Δ ->
  (Γ.cons H).is_upgrade (Δ.cons H')

@[aesop safe]
theorem Term.Ctx.is_upgrade_refl (Γ: Ctx): Γ.is_upgrade Γ := by induction Γ <;> aesop
theorem Term.Ctx.is_upgrade_refl' (Γ Δ: Ctx) (p: Γ = Δ): Γ.is_upgrade Δ := p ▸ Γ.is_upgrade_refl

@[aesop unsafe 50%]
theorem Term.Ctx.is_upgrade.trans {Γ Δ Ξ: Ctx} (H: Γ.is_upgrade Δ) (H': Δ.is_upgrade Ξ)
  : Γ.is_upgrade Ξ
  := by induction H generalizing Ξ <;> aesop
theorem Term.Ctx.is_upgrade.antisymm {Γ Δ: Ctx} (H: Γ.is_upgrade Δ) (H': Δ.is_upgrade Γ): Γ = Δ
  := by induction H <;> aesop

instance upgrade_partial_ord: PartialOrder Term.Ctx := {
  le := Term.Ctx.is_upgrade,
  le_refl := Term.Ctx.is_upgrade_refl,
  le_trans := @Term.Ctx.is_upgrade.trans,
  le_antisymm := @Term.Ctx.is_upgrade.antisymm
}

@[aesop [unsafe 50% cases, safe constructors]]
inductive Term.Ctx.is_upgrade_t: Ctx -> Ctx -> Type
| nil: nil.is_upgrade_t nil
| cons {Γ Δ: Ctx} {H H': Hyp}: H.is_upgrade H' -> Γ.is_upgrade_t Δ ->
  (Γ.cons H).is_upgrade_t (Δ.cons H')

@[aesop safe]
theorem Term.Ctx.is_upgrade_t.coherent {Γ Δ: Ctx} (H H': Γ.is_upgrade_t Δ) : H = H'
  := by induction H <;> cases H' <;> aesop

@[aesop [unsafe 50% cases, safe constructors]]
inductive Term.Ctx.is_upgrade_k (Γ Δ: Ctx): Kind -> Prop
| ty: Γ = Δ -> Γ.is_upgrade_k Δ ty
| pr: Γ.is_upgrade Δ -> Γ.is_upgrade_k Δ pr
| gs: Γ.is_upgrade Δ -> Γ.is_upgrade_k Δ gs

@[aesop safe]
theorem Term.Ctx.is_upgrade_k_refl (Γ: Ctx) (k: Kind): Γ.is_upgrade_k Γ k
  := by cases k <;> aesop
theorem Term.Ctx.is_upgrade_k_refl' (Γ Δ: Ctx) (k: Kind) (p: Γ = Δ): Γ.is_upgrade_k Δ k
  := p ▸ Γ.is_upgrade_k_refl k
@[aesop unsafe 50%]
theorem Term.Ctx.is_upgrade_k_trans (Γ Δ Ξ: Ctx) (k: Kind) (p: Γ = Δ)
  (H: Γ.is_upgrade_k Δ k) (H': Δ.is_upgrade_k Ξ k): Γ.is_upgrade_k Ξ k
  := by aesop
@[aesop unsafe 50%]
theorem Term.Ctx.is_upgrade_k_upgrade (Γ Δ: Ctx) (k k': Kind) (Hk: k.is_upgrade k')
  (H: Γ.is_upgrade_k Δ k): Γ.is_upgrade_k Δ k'
  := by aesop
theorem Term.Ctx.is_upgrade_k.antisymm {Γ Δ: Ctx} {k k': Kind}
  (H: Γ.is_upgrade_k Δ k) (H': Δ.is_upgrade_k Γ k'): Γ = Δ
  := by cases H <;> cases H' <;> aesop (add 50% Term.Ctx.is_upgrade.antisymm)

-- inductive Term.Ctx.is_upgrade_cases_het: Ctx -> Ctx -> Type
-- | same_hyp (Γ Δ: Ctx) (k A): Γ.is_upgrade Δ ->
--   (Γ.cons_kind k A).is_upgrade_cases_het (Δ.cons_kind k A)
-- | gs_hyp (Γ Δ: Ctx) (A): Γ.is_upgrade Δ -> (Γ.cons_ty A).is_upgrade_cases_het (Δ.cons_gs A)

-- inductive Term.Ctx.is_upgrade_het: Ctx -> Ctx -> Type
-- | nil: nil.is_upgrade_het nil
-- | same_hyp (Γ Δ: Ctx) (k A): Γ.is_upgrade_het Δ ->
--   (Γ.cons_kind k A).is_upgrade_het (Δ.cons_kind k A)
-- | gs_hyp (Γ Δ: Ctx) (A): Γ.is_upgrade_het Δ -> (Γ.cons_ty A).is_upgrade_het (Δ.cons_gs A)
