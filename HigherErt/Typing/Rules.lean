import HigherErt.Syntax.Term
import HigherErt.Syntax.Subst
import HigherErt.Typing.Ctx

open Term

@[aesop [safe constructors]]
inductive Term.has_type: Ctx -> Term -> Term -> Prop
-- Variables
| var (Γ: Ctx) (n: Nat) (A: Term) (k: Kind) (K: Term):
  k.var_kind K ->
  has_type Γ A K ->
  Γ.has_var n k A ->
  has_type Γ (var n) A

-- Type formers
| pi (Γ: Ctx) (A B: Term):
  has_type Γ A type ->
  has_type (Γ.cons_ty A) B type ->
  has_type Γ (pi ty ty A B) type
| sigma (Γ: Ctx) (A B: Term):
  has_type Γ A type ->
  has_type (Γ.cons_ty A) B type ->
  has_type Γ (sigma ty ty A B) type
| coprod (Γ: Ctx) (A B: Term):
  has_type Γ A type ->
  has_type Γ B type ->
  has_type Γ (coprod ty A B) type
| set (Γ: Ctx) (A φ: Term):
  has_type Γ A type ->
  has_type (Γ.cons_ty A) φ prop ->
  has_type Γ (sigma ty pr A φ) type
| asm (Γ: Ctx) (A φ: Term):
  has_type Γ φ prop ->
  has_type (Γ.cons_ty φ) A type ->
  has_type Γ (pi pr ty φ A) type
| intersect (Γ: Ctx) (A B: Term):
  has_type Γ A type ->
  has_type (Γ.cons_ty A) B type ->
  has_type Γ (pi gs ty A B) type
| union (Γ: Ctx) (A B: Term):
  has_type Γ A type ->
  has_type (Γ.cons_ty A) B type ->
  has_type Γ (sigma gs ty A B) type
| unit (Γ: Ctx): has_type Γ unit type
| prop (Γ: Ctx): has_type Γ prop type

-- Proposition formers
| bot (Γ: Ctx): has_type Γ bot prop
| top (Γ: Ctx): has_type Γ top prop
| imp (Γ: Ctx):
  has_type Γ φ prop ->
  has_type (Γ.cons_pr φ) ψ prop ->
  has_type Γ (pi pr pr φ ψ) prop
| conj (Γ: Ctx):
  has_type Γ φ prop ->
  has_type (Γ.cons_pr φ) ψ prop ->
  has_type Γ (sigma pr pr φ ψ) prop
| disj (Γ: Ctx) (φ ψ: Term):
  has_type Γ φ type ->
  has_type Γ ψ type ->
  has_type Γ (coprod pr φ ψ) type
| forall (Γ: Ctx) (A φ: Term):
  has_type Γ A type ->
  has_type (Γ.cons_ty A) φ prop ->
  has_type Γ (pi gs pr A φ) prop
| exists (Γ: Ctx) (A φ: Term):
  has_type Γ A type ->
  has_type (Γ.cons_ty A) φ prop ->
  has_type Γ (sigma gs pr A φ) prop

-- Term/proof formers
--NOTE: this allows a let-binding in a type definition, but there's no type former for this yet
| let_ (Γ: Ctx) (k: Kind) (e A K e' B: Term):
  has_type Γ A K ->
  k.up_kind K ->
  Γ.is_upgrade_k Δ k ->
  has_type Δ e A ->
  has_type (Γ.cons_kind k A) e' B ->
  has_type Γ (let_ k e e') (B.subst0 e)
| lam (Γ: Ctx) (k k': Kind) (A K t B K': Term):
  has_type Γ A K ->
  has_type Γ B K' ->
  k.up_kind K ->
  k'.var_kind K' ->
  has_type (Γ.cons_kind k A) t B ->
  has_type Γ (lam k A t) (pi k k' A B)
| app (Γ Δ: Ctx) (k k': Kind) (A K B s t: Term):
  -- has_type Γ A K ->
  -- k.var_kind K ->
  has_type Γ s (pi k k' A B) ->
  Γ.is_upgrade_k Δ k ->
  has_type Δ t A ->
  has_type Γ (app k s t) B
| pair (Γ ΓA ΓB: Ctx) (kA kB: Kind) (A K B l r: Term):
  Γ.is_upgrade_k ΓA kA ->
  (Γ.cons_kind kA a).is_upgrade_k ΓB kB ->
  has_type ΓA l A ->
  has_type ΓB r B ->
  has_type Γ (pair kA kB l r) (sigma kA kB A B)
--TODO: pair
--TODO: let_pair
--TODO: inj
--TODO: case
--TODO: abort

-- Base terms/proofs
--TODO: nil
--TODO: triv

-- Theory of equality
--TODO: eq
--TODO: refl
--TODO: sbst
--TODO: reduce
--TODO: irir
--TODO: prir
--TODO: unit_unique
--TODO: discr

-- Theory of natural numbers
--TODO: nat
--TODO: zero
--TODO: succ
--TODO: natrec

--TODO: has_type syntax

notation:10 Γ " ⊢ " a " : " A => Term.has_type Γ a A

set_option trace.aesop.steps true

-- theorem Term.has_type.upgrade {Γ Δ} {a A: Term} (H: Γ ⊢ a: A) (HΓΔ: Γ.is_upgrade Δ)
--   : Δ ⊢ a: A
--   := by {
--     induction H
--     case var => aesop
--     all_goals sorry
--   }
